﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyGenerator : MonoBehaviour
{
    public GameObject enemyPrefab;
    float delta = 0.5f;
    float span = 1.0f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;//プレイ時間

        Debug.Log(delta);

        if (delta > span)

        {
            delta = 0;

            span = span *0.999f;

            GameObject go = Instantiate(enemyPrefab);
            //矢を作り、それをgoという変数とする

            float px = Random.Range(-5.0f, 5.0f);//-9から9までのランダムな数字(少数も)

            go.transform.position = new Vector3(8, px, 0);//goを()に出現させる
        }
    }
}
