﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class item3 : MonoBehaviour
{
    float X = -0.02f;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(X, 0, 0);

        if (transform.position.x < -9.5)//-5より低くなったら
        {
            Destroy(gameObject);//自分を消す
        }

        //当たり判定を作る
        Vector2 p1 = transform.position;//矢の中心座標(ベクトル)
        Vector2 p2 = player.transform.position;//プレイヤーの中心座標(ベクトル)
        Vector2 dir = p1 - p2;//矢とプレイヤーの中心座標の距離(ベクトル)
        float d = dir.magnitude;//上の長さ
        float r1 = 0.3f;//矢の当たり判定
        float r2 = 1.3f;//プレイヤーの当たり判定

        if (d < r1 + r2)
        {
            Destroy(gameObject);//矢を消す

            GameObject gameDirector = GameObject.Find("GOD");
            gameDirector.GetComponent<GOD>().DecreaseHp1();//
            //hpゲージにアクセスsuru
        }
    }
}
