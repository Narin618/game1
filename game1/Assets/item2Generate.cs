﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class item2Generate : MonoBehaviour
{
    public GameObject item2Prefab;
    float delta1 = 0.2f;
    float span1 = 10.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta1 += Time.deltaTime;//プレイ時間

        Debug.Log(delta1);

        if (delta1 > span1)

        {
            delta1 = 0;

            span1 = span1 * 0.999f;

            GameObject go = Instantiate(item2Prefab);
            //矢を作り、それをgoという変数とする

            float px = Random.Range(-5.0f, 5.0f);//-9から9までのランダムな数字(少数も)

            go.transform.position = new Vector3(8, px, 0);//goを()に出現させる
        }
    }
}
