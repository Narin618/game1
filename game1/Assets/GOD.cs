﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class GOD : MonoBehaviour
{
    GameObject hpGauge;
    int hp=10;
    // Start is called before the first frame update
    void Start()
    {
        hpGauge = GameObject.Find("hpgauge");//ｈｐゲージを探す
    }

    // Update is called once per frame
    void Update()
    {
        if(hp<=0)
        {
            SceneManager.LoadScene("gameover");
        }
    }

    public void DecreaseHp()
    {
        hpGauge.GetComponent<Image>().fillAmount -= 0.1f; //ｈｐゲージを減らす
        hp--;
    }

    public void DecreaseHp1()
    {
        hpGauge.GetComponent<Image>().fillAmount += 0.1f; //ｈｐゲージをふやす
        if(hp<10)
        {
            hp++;
        }
    }
}
